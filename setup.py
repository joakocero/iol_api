#!/usr/bin/env python3
from setuptools import setup, find_packages
import shlex
from subprocess import check_output, CalledProcessError

def git_to_pep440(git_version):
    if '-' not in git_version:
        return git_version

    sep = git_version.index('-')
    version = git_version[:sep] + '+dev' + git_version[sep+1:].replace('-', '.')
    return version


def git_version():
    cmd = 'git describe --tags --always --dirty --match v[0-9]*'
    try:
        git_version = check_output(shlex.split(cmd)).decode('utf-8').strip()[1:]
    except CalledProcessError as e:
        raise Exception('Could not get git version') from e
    return git_to_pep440(git_version)


setup(
    name='iol_api',
    version=git_version(),
    description='Python wrapper for InvertirOnline API',
    long_description='',
    url='https://gitlab.com/joakocero/iol_api',
    author='Joaquin Miranda',
    license="?",
    author_email='joaquin.maximiliano.miranda@gmail.com',
    classifiers=[
        'Environment :: Console',
        'Operating System :: Linux',
        'Programming Language :: Python',
    ],
    # Beware of using find_packages.
    packages=find_packages(),
    install_requires=[
        "requests"
    ],
    scripts=[],
    # data_files=comming,
)
