PARAMETER_TYPE_CONV = {"s": str,
                       "i": int,
                       "c": chr,
                       "f": (float, int)}

def is_parameter_type_ok(parameter_type, value):
    return isinstance(value, PARAMETER_TYPE_CONV[parameter_type])

def process_extra_parameters(required_parameters, kw_parameters):
    for parameter in required_parameters:
        if not parameter.get("required", False):
            continue
        parameter_name = parameter["name"]
        parameter_type = parameter["type"]
        parameter_options = parameter.get("options", [])
        if parameter_options and not kw_parameters[parameter_name] in parameter_options:
            raise ValueError(parameter_name + " accept only " + str(parameter_options) +
                             " as options.")
        if not is_parameter_type_ok(parameter_type, kw_parameters[parameter_name]):
            raise TypeError(parameter_name + " require " +
                            str(PARAMETER_TYPE_CONV[parameter_type]) + " type.")
