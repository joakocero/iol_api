""" IOL API V2 """
API_BASE_ADDRESS = "https://api.invertironline.com"

API_ENDPOINTS = {
    "account": {
        "name": "cuenta",
        "childs": {
            "account_state": {
                "name": "estado",
                "endpoint": "api/v2/estadocuenta",
                "method": "GET"
            },
            "portfolio": {
                "name": "portfolio",
                "endpoint": "api/v2/portafolio/{pais}",
                "method": "GET"
            },
            "trades": {
                "name": "operaciones",
                "endpoint": "api/v2/operaciones",
                "method": "GET"
            }
        }
    },
    "securities": {
        "name": "titulos",
        "endpoint": "api/v2/{mercado}/Titulos/{simbolo}",
        "method": "GET",
        "childs": {
            "fci": {
                "name": "fci",
                "endpoint": "api/v2/Titulos/FCI",
                "method": "GET",
                "childs": {
                    "symbol": {
                        "name": "simbolo",
                        "endpoint": "api/v2/Titulos/FCI/{simbolo}",
                        "method": "GET"
                    },
                    "fund_types": {
                        "name": "tipo_fondos",
                        "endpoint": "api/v2/Titulos/FCI/TipoFondos",
                        "method": "GET"
                    },
                    "fund_managers": {
                        "name": "administradoras",
                        "endpoint": "api/v2/Titulos/FCI/Administradoras",
                        "method": "GET"
                    }
                }
            },
            "intruments": {
                "name": "instrumentos",
                "endpoint": "api/v2/{pais}/Titulos/Cotizacion/Instrumentos",
                "method": "GET",
                "childs": {
                    "board": {
                        "name": "paneles",
                        "endpoint": "api/v2/{pais}/Titulos/Cotizacion/Paneles/{instrumento}",
                        "method": "GET"
                    }
                }
            }
        }
    },
    "trade": {
        "name": "operar",
        "childs": {
            "buy": {
                "name": "comprar",
                "endpoint": "api/v2/operar/Comprar",
                "method": "POST",
                "parameters": [{"name": "mercado", "type": 's', "required": True,
                                "options": ['bCBA', 'nYSE', 'nASDAQ', 'aMEX', 'bCS', 'rOFX']},
                               {"name": "simbolo", "type": 's', "required": True},
                               {"name": "cantidad", "type": 'i', "required": True},
                               {"name": "precio", "type": 'f', "required": True},
                               {"name": "plazo", "type": 's', "required": True,
                                "options": ['t0', 't1', 't2']},
                               {"name": "validez", "type": 's', "required": True}]
            }
        }
    },
}
