import os
import re
import requests
from iol.utils import process_extra_parameters
from iol.token_mgr import IOLToken
from iol import api_endpoints as api_config
# pylint: disable=protected-access


class IOL(IOLToken):
    def __init__(self, auth_config):
        IOLToken.__init__(self, auth_config)
        self._base_address = api_config.API_BASE_ADDRESS
        self._make_api_calls(api_config.API_ENDPOINTS)

    def _make_api_calls(self, config):
        for conf in config:
            self.__setattr__(config[conf]["name"], self._APICall(self, config[conf]))

    class _APICall():
        def __init__(self, parent_instance, config):
            self._parent_instance = parent_instance
            childs = config.get("childs", [])
            for child in childs:
                self.__setattr__(childs[child]["name"],
                                 self._parent_instance._APICall(self._parent_instance,
                                                                childs[child]))
            if ("endpoint" in config) and ("method" in config):
                self.__setattr__(config["method"].lower(),
                                 self._make_api_function(config))

        def _get_url(self, endpoint, parameters):
            endpoint_parameters = re.findall(r'({(\w+)})', endpoint)
            for parameter in endpoint_parameters:
                endpoint = endpoint.replace(parameter[0], parameters[parameter[1]])
            return os.path.join(self._parent_instance._base_address, endpoint)


        def _make_api_function(self, config):
            def _make_call(**kwargs):
                endpoint_parameters = re.findall(r'{(\w+)}', config["endpoint"])
                extra_parameters = [parameter["name"]
                                    for parameter in config.get("parameters", [])]
                full_parameters = endpoint_parameters + extra_parameters
                if not all(key in kwargs.keys() for key in full_parameters):
                    print("Required parameters:", full_parameters)
                    return
                process_extra_parameters(config.get("parameters", []), kwargs)
                url = self._get_url(config["endpoint"], kwargs)
                headers = {"Authorization": "Bearer " + self._parent_instance._access_token}
                api_key = {"api_key": self._parent_instance._access_token}
                if config["method"] == "GET":
                    rsp = requests.get(url=url, headers=headers,
                                       params=api_key)
                elif config["method"] == "POST":
                    rsp = requests.post(url=url, headers=headers,
                                        data=kwargs.update(api_key))
                else:
                    print("Wrong method")
                    return
                if rsp.status_code != 200:
                    print("Request fail:", rsp.text, "\nStatus code:", rsp.status_code)
                return rsp.json()
            _make_call.__name__ = config["name"]
            return _make_call
