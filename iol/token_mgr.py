from time import sleep
import threading
import requests

class IOLToken():
    def __init__(self, config):
        self.__config = config
        self._access_token = None
        self._token_data = None
        self._get_token()
        self._run_thread()

    def _get_token(self):
        rsp = requests.post(self.__config.TOKEN_URL, data=self.__config.PAYLOAD)
        if rsp.status_code != 200:
            print("Fail getting token, check the internet connection")
        self._token_data = rsp.json()
        self._access_token = self._token_data["access_token"]

    def _refresh_token(self):
        rsp = requests.post(self.__config.TOKEN_URL,
                            data={"refresh_token": self._token_data["refresh_token"],
                                  "grant_type": "refresh_token"})
        if rsp.status_code != 200:
            print("Fail refreshing token, getting a new one")
        self._token_data = rsp.json()
        self._access_token = self._token_data["access_token"]

    def _run_thread(self):
        def _run(run_event):
            try:
                while run_event.is_set():
                    self._refresh_token()
                    sleep(self._token_data.get("expires_in", 60) - 30)
            except KeyboardInterrupt:
                run_event.clear()
                thread.join()
        run_event = threading.Event()
        run_event.set()
        thread = threading.Thread(target=_run, args=(run_event,))
        thread.daemon = True
        thread.start()
